#!/usr/bin/python3
"""
    Permission to use, copy, modify, and/or distribute this software for
    any purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED “AS IS” AND THE AUTHOR DISCLAIMS ALL
    WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES
    OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE
    FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY
    DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
    AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
    OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
"""
# NOTE: updating $PATH requires restarting the DE
import os
import subprocess

home_dir = os.getenv("HOME")

if home_dir == None:
    exit(1)

ROOT_DESKTOP_FOLDER = "/usr/share/applications/"
USER_DESKTOP_FOLDER = home_dir + "/.local/share/applications/"

def get_cmd_result(cmd):
    o = subprocess.check_output(cmd, shell=True)
    return o.decode("utf-8")

def get_executables():
    env_path = os.getenv("PATH")

    if env_path == None:
        return set()

    paths = env_path.split(os.pathsep)
    executables = set()

    for path in paths:
        dir = ""
        try:
            dir = os.listdir(path)
        except FileNotFoundError:
            continue

        for filename in dir:
            file_path = os.path.join(path, filename)
            if os.path.isfile(file_path) and os.access(file_path, os.X_OK):
                executables.add(filename)

    return list(executables)

def get_desktop_files(folder):
    result = []
    all_files = os.listdir(folder)
    
    for file in all_files:
        if file.endswith(".desktop"):
            result.append(file)

    return result

def list_to_dmenu_string(lists):
    r = ""
    for l in lists:
        for i in l:
            r += i + "\n"
    return r

def get_flatpak_programs():
    fp_l = get_cmd_result("flatpak list --app")
    lines = fp_l.splitlines()
    program_ids = []
    for l in lines:
        # The command outputs something like:
        # app  \t    app.id    \t    [things we don't care about]
        tokens = l.split("\t")
        id = tokens[1]
        program_ids.append(id)
    return program_ids
        

executables = get_executables()
root_desktop_files = get_desktop_files(ROOT_DESKTOP_FOLDER)
user_desktop_files = get_desktop_files(USER_DESKTOP_FOLDER)
flatpak_programs = get_flatpak_programs()

dmenu_lists = []
dmenu_lists.append(executables)
dmenu_lists.append(root_desktop_files)
dmenu_lists.append(user_desktop_files)
dmenu_lists.append(flatpak_programs)

dmenu_selection = get_cmd_result("echo \"" + list_to_dmenu_string(dmenu_lists) + "\" | dmenu").strip()

if dmenu_selection in flatpak_programs:
    subprocess.run(["flatpak", "run", dmenu_selection])
elif dmenu_selection in executables:
    subprocess.run([dmenu_selection])
else:
    f = (ROOT_DESKTOP_FOLDER if
            dmenu_selection in root_desktop_files else USER_DESKTOP_FOLDER)
    cmd = ["gio", "launch", f + dmenu_selection]
    print(cmd)
    subprocess.run(cmd)


